import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";
import Calculator from "./Calculator/CalculatorReact";
import Slider from "./Slider/slider";
import img1 from "./Slider/img/1(small).jpg";
import img2 from "./Slider/img/2(small).jpg";
import img3 from "./Slider/img/3(small).jpg";
import img4 from "./Slider/img/4(small).jpg";
import img5 from "./Slider/img/5(small).jpg";

let images1 = [
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2xqG_emTuoGxuq0KofeTSwtwH707CDf71ovo6gDrHIP7gEUZqnQ",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPBx_JUJankLdxtchDb2riOpv4DXe63nI90Vi1wkeM78tSM1Y7",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIcOKL62MyaJS3fhrJrvFTxXyHE-I2A0Z_hvaOQ2kBy3CL9_4S",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVznYppxxRNfc_kiXXCWdAxCbg6ujjN8jR44Dpi2YqCbpJMRIp"
];
let images2 = [img1, img2, img3, img4, img5];
ReactDOM.render(
  <div>
    <Calculator />
    <Slider images={images1} index={0} />
    <Slider images={images2} index={1} />
  </div>,

  document.getElementById("root")
);
registerServiceWorker();
