import React, { Component } from "react";
import "./calculator.css";

class Calculator extends Component {
  constructor() {
    super();
    this.state = {
      operation: "",
      numberStart: "",
      firstNumber: "",
      result: 0,
      secondNumber: ""
    };
  }
  getOperation(e) {
    let { numberStart } = this.state;
    this.setState({
      numberStart: "",
      operation: e.currentTarget.name,
      firstNumber: +numberStart
    });
  }
  getNumber(e) {
    let { numberStart } = this.state;
    this.setState({
      numberStart: numberStart + e.currentTarget.name,
      secondNumber: +(numberStart + e.currentTarget.name)
    });
  }
  getResult() {
    let { firstNumber, secondNumber, operation } = this.state;
    let result = 0;
    switch (operation) {
      case "+":
        result = Math.round((firstNumber + secondNumber) * 100) / 100 
        break;
      case "-":
        result = Math.round((firstNumber - secondNumber) * 100) / 100;
        break;
      case "*":
        result = Math.round((firstNumber * secondNumber) * 100) / 100;
        break;
      case "/":
        result = Math.round((firstNumber / secondNumber) * 100) / 100;
        break;
      default:
        break;
    }
    this.setState({
      numberStart: result
    });
    
    console.log("firstNumber" + firstNumber);
    console.log("secondNumber" + secondNumber);
  }
  changeSign(){
    let { numberStart } = this.state;
    this.setState({numberStart: -numberStart});      
  }
  deleteResult() {
    this.setState({numberStart: ""});
  }

  render() {
    return (
      <div className="Calculator">
        <input value={this.state.numberStart}
              className="input_value_calculate"
              onChange={this.getNumber.bind(this)}/>
        <div className="btn_wrapper">
          <div className="block_btn">
            <button className="delete_value" onClick={this.deleteResult.bind(this)}> C </button>
            <button name="/" className="operators" onClick={this.getOperation.bind(this)}> / </button>
          </div>
          <div className="block_btn">
            <button name="7" className="number" onClick={this.getNumber.bind(this)}> 7 </button>
            <button name="8" className="number" onClick={this.getNumber.bind(this)}> 8 </button>
            <button name="9" className="number" onClick={this.getNumber.bind(this)}> 9 </button>
            <button name="*" className="operators" onClick={this.getOperation.bind(this)}> * </button>
          </div>
          <div className="block_btn">
            <button name="4" className="number" onClick={this.getNumber.bind(this)}> 4 </button>
            <button name="5" className="number" onClick={this.getNumber.bind(this)}> 5 </button>
            <button name="6" className="number" onClick={this.getNumber.bind(this)}> 6 </button>
            <button name="-" className="operators" onClick={this.getOperation.bind(this)}> - </button>
          </div>
          <div className="block_btn">
            <button name="1" className="number" onClick={this.getNumber.bind(this)}> 1 </button>
            <button name="2" className="number" onClick={this.getNumber.bind(this)}> 2 </button>
            <button name="3" className="number" onClick={this.getNumber.bind(this)}> 3 </button>
            <button name="+" className="operators" onClick={this.getOperation.bind(this)}> + </button>
          </div>
          <div className="block_btn">
            <button name="+-" className="plus_minus" onClick={this.changeSign.bind(this)}> +- </button>
            <button name="0" className="number" onClick={this.getNumber.bind(this)}> 0 </button>
            <button name="." className="number" onClick={this.getNumber.bind(this)}> . </button>
            <button name="=" className="result" onClick={this.getResult.bind(this)}> = </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Calculator;
