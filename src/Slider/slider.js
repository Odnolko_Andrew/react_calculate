import React, { Component } from "react";
import "./slider.css";

class Slider extends Component {
  constructor(props) {
    super();
    this.state = {
      images: props.images,
      currentImageIndex: props.index,
      isCycleMode: false,
      canGoPrev: props.index > 0,
      canGoNext: props.index < props.images.length - 1
    };
    this.nextSlideHandler = this.nextSlideHandler.bind(this);
  }
  _makeNextBtn(currentIndex) {
    let nextIndex = currentIndex;
    if (nextIndex < this.state.images.length - 1) {
      nextIndex = this.state.currentImageIndex + 1;
      this.setState({ canGoPrev: true });
    }

    if (nextIndex === this.state.images.length - 1) {
      this.setState({ canGoNext: false });
    }
    return nextIndex;
  }
  _makePrevBtn(currentIndex) {
    let nextIndex = currentIndex;
    if (nextIndex > 0) {
      nextIndex = this.state.currentImageIndex - 1;
      this.setState({ canGoNext: true });
    }
    if (nextIndex === 0) {
      this.setState({ canGoPrev: false });
    }
    return nextIndex;
  }

  nextSlideHandler(e) {
    let currentIndex = this.state.currentImageIndex;
    let nextIndex = currentIndex;
    if (e.currentTarget.dataset.direction === "next") {
      nextIndex = this._makeNextBtn(currentIndex);
    } else {
      nextIndex = this._makePrevBtn(currentIndex);
    }
    this.setState({ currentImageIndex: nextIndex });
  }

  render() {
    return (
      <div className="slider">
        <div>
          <button
            disabled={!this.state.canGoPrev}
            data-direction="prev"
            onClick={this.nextSlideHandler}
          >
            prev
          </button>
        </div>
        <div>
          <img src={this.state.images[this.state.currentImageIndex]} alt="" />
        </div>
        <div>
          <button
            disabled={!this.state.canGoNext}
            data-direction="next"
            onClick={this.nextSlideHandler}
          >
            next
          </button>
        </div>
      </div>
    );
  }
}

export default Slider;
